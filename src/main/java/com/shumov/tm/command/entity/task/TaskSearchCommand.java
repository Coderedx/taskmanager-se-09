package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TaskSearchCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "task-search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Task search";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SEARCH]");
        System.out.println("enter a search term:".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String term = terminalService.nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectInputData(term);
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final List<Task> list = taskService.getTaskList(user.getId());
        @NotNull final List<Task> listResult = new ArrayList<>();
        for (@NotNull final Task task : list) {
            if(task.getName().contains(term) || task.getDescription().contains(term)){
                listResult.add(task);
            }
        }
        System.out.println("[SEARCH RESULT]");
        for (@NotNull final Task task : listResult) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName() +
                    "\nDATE CREATE: " + formatter.format(task.getDateCreated()) +
                    "\nDATE START: " + formatter.format(task.getDateStart()) +
                    "\nDATE FINISH: " + formatter.format(task.getDateFinish()) +
                    "\nSTATUS: " + task.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
