package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.User;
import com.shumov.tm.comparator.entity.ProjectComparator;
import com.shumov.tm.enumerate.SortMethod;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ProjectListSortedCommand extends AbstractCommand {

    @NotNull
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    public @NotNull String command() {
        return "project-list-sorted";
    }

    @Override
    public @NotNull String getDescription() {
        return "Sorted project list";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SORTED LIST]");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        System.out.println("[How do you want to sort the list?]".toUpperCase());
        System.out.println("create : sort by create date");
        System.out.println("start : sort by start date");
        System.out.println("finish : sort by finish date");
        System.out.println("status : sort by start date");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String method = terminalService.nextLine();
        @NotNull final User user = serviceLocator.getCurrentUser();
        @NotNull final List<Project> list = projectService.getProjectList(user.getId());
        sort(method,list);
        for (@NotNull final Project project : list) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName() +
                    "\nDATE CREATE: " + formatter.format(project.getDateCreated()) +
                    "\nDATE START: " + formatter.format(project.getDateStart()) +
                    "\nDATE FINISH: " + formatter.format(project.getDateFinish()) +
                    "\nSTATUS: " + project.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void sort(@NotNull final String method,
                      @NotNull final List<Project> list){
        switch (method.toLowerCase()){
            case "create" :
                list.sort(new ProjectComparator(SortMethod.BY_CREATE_DATE));
                break;
            case "start" :
                list.sort(new ProjectComparator(SortMethod.BY_START_DATE));
                break;
            case "finish" :
                list.sort(new ProjectComparator(SortMethod.BY_FINISH_DATE));
                break;
            case "status" :
                list.sort(new ProjectComparator(SortMethod.BY_STATUS));
                break;
                default:
                    System.out.println("[you have not selected a sorting method]".toUpperCase());
        }
    }
}
