package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.Project;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class ProjectAdminListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-list-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT LIST]");
        @NotNull final IProjectService projectService = serviceLocator.getProjectService();
        for (@NotNull final Project project : projectService.getProjectList()) {
            System.out.println("PROJECT ID: " + project.getId() +"\n"
                    + "PROJECT NAME: " + project.getName() + "\n" +
                    "USER ID: " + project.getOwnerId()+ "\n");
        }
    }
}
