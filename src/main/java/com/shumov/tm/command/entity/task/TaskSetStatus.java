package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskSetStatus extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "task-set-status";
    }

    @Override
    public @NotNull String getDescription() {
        return "Set task status";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SET STATUS]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        System.out.println("ENTER TASK STATUS:");
        System.out.println("planned : task planned");
        System.out.println("progress : task in progress");
        System.out.println("done : task done");
        @NotNull final String status = terminalService.nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final ExecutionStatus taskStatus = taskService.taskStatus(status);
        @NotNull final User user = serviceLocator.getCurrentUser();
        taskService.editTaskStatus(user.getId(), id, taskStatus);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.USER);
        roleTypes.add(UserRoleType.ADMIN);
    }
}
