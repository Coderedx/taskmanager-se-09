package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.enumerate.UserRoleType;
import org.jetbrains.annotations.NotNull;

public class TaskAdminEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-edit-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change name of any task";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(UserRoleType.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = serviceLocator.getTerminalService().nextLine();
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        taskService.isCorrectInputData(taskId);
        System.out.println("ENTER NEW TASK NAME:");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        taskService.isCorrectInputData(taskName);
        taskService.editTaskNameById(taskId,taskName);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
